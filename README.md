# Services

This project contains necessary for ideen project docker services.

## Run

Up zookeeper, kafka, kafka-ui, kowl, mongodb and postgres:

```
docker-compose up -f docker-compose-kafka.yml -d
```

Up airflow ecosystem:

```
docker-compose up -f docker-compose-airflow.yml -d
```
